package com.hw.db.dao;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Random;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.argThat;


public class PostDaoTest {

    JdbcTemplate jdbcMock = Mockito.mock(JdbcTemplate.class);
    PostDAO postDAO = new PostDAO(jdbcMock);
    Random random = new Random();
    Post existingPost;

    @BeforeEach
    void setUp() {
        this.existingPost = getPost(
                RandomString.make(10),
                RandomString.make(10),
                Timestamp.valueOf(LocalDateTime.now().plusHours(random.nextInt(10))),
                random.nextInt(10));

        Mockito.when(jdbcMock.queryForObject(
                        Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                        Mockito.any(PostDAO.PostMapper.class),
                        Mockito.any()))
                .thenReturn(existingPost);
    }

    static Stream<Arguments> postDaoTestArguments() {
        Random random = new Random();

        return Stream.of(
                Arguments.of(RandomString.make(10), null, null, "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Arguments.of(null, RandomString.make(10), null, "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Arguments.of(null, null, LocalDateTime.now().minusHours(random.nextInt(10)), "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(RandomString.make(10), RandomString.make(10), null, "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Arguments.of(RandomString.make(10), RandomString.make(10), LocalDateTime.now().minusHours(random.nextInt(10)), "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(null, RandomString.make(10), LocalDateTime.now().minusHours(random.nextInt(10)), "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(RandomString.make(10), null, LocalDateTime.now().minusHours(random.nextInt(10)), "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(null, null, null, null)
        );
    }

    private Post getPost(String author,
                         String message,
                         Timestamp created,
                         Integer id) {
        Post post = new Post();
        post.setAuthor(author);
        post.setMessage(message);
        post.setCreated(created);
        post.setId(id);
        return post;
    }
    
    @ParameterizedTest
    @MethodSource("postDaoTestArguments")
    void postDaoTest(String author, String message, LocalDateTime created, String expectedQuery) {
        Post post2 = getPost(
                author == null ? existingPost.getAuthor() : author,
                message == null ? existingPost.getMessage() : message,
                created == null ? existingPost.getCreated() : Timestamp.valueOf(created),
                existingPost.getId());

        postDAO.setPost(existingPost.getId(), post2);

        if (expectedQuery == null) {
            Mockito.verify(jdbcMock, Mockito.never()).update(Mockito.anyString(), Mockito.any(Object[].class));
        } else {
            Mockito.verify(jdbcMock).update(Mockito.eq(expectedQuery), (Object[]) Mockito.any());
        }
    }

}
