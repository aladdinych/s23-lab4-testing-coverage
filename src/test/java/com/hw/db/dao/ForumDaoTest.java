package com.hw.db.dao;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.stream.Stream;

public class ForumDaoTest {

    JdbcTemplate jdbcMock = Mockito.mock(JdbcTemplate.class);
    ForumDAO forumDao;

    @BeforeEach
    void init() {
        this.forumDao = new ForumDAO(jdbcMock);
    }

    static Stream<Arguments> userListTestArguments() {
        return Stream.of(
                Arguments.of(null, null, false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
                Arguments.of(10, null, false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"),
                Arguments.of(10, "05.10.2000", false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
                Arguments.of(null, "05.10.2000", false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                Arguments.of(null, null, true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;"),
                Arguments.of(10, null, true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Arguments.of(10, "05.10.2000", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Arguments.of(null, "05.10.2000", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;")
        );
    }

    @ParameterizedTest
    @MethodSource("userListTestArguments")
    void forumUserListTest(Integer limit, String since, boolean desc, String expectedQuery) {
        forumDao.UserList("any", limit, since, desc);
        Mockito.verify(jdbcMock).query(
                Mockito.eq(expectedQuery),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }
    
}
