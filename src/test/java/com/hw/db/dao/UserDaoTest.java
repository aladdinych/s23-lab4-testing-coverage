package com.hw.db.dao;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import net.bytebuddy.utility.RandomString;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class UserDaoTest {

    JdbcTemplate jdbcMock = Mockito.mock(JdbcTemplate.class);
    UserDAO userDAO = new UserDAO(jdbcMock);

    static Stream<Arguments> userDaoTestArguments() {
        return Stream.of(
                Arguments.of("fullname", "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                Arguments.of("email", "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                Arguments.of("about", "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;")
        );
    }

    @ParameterizedTest
    @MethodSource("userDaoTestArguments")
    void testUserDaoUpdate(String fieldToUpdate, String expectedQuery) {
        User user = getUser();

        if ("fullname".equals(fieldToUpdate)) {
            user.setFullname(RandomString.make(10));
        } else if ("email".equals(fieldToUpdate)) {
            user.setEmail(RandomString.make(10));
        } else if ("about".equals(fieldToUpdate)) {
            user.setAbout(RandomString.make(10));
        }

        userDAO.Change(user);

        Mockito.verify(jdbcMock).update(
                Mockito.eq(expectedQuery),
                Mockito.anyString(),
                Mockito.anyString()
        );
    }

    private User getUser() {
        User user = new User();
        user.setNickname(RandomString.make(10));
        return user;
    }
}
