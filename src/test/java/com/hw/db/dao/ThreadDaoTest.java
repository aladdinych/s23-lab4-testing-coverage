package com.hw.db.dao;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class ThreadDaoTest {

    JdbcTemplate jdbcMock = Mockito.mock(JdbcTemplate.class);
    ThreadDAO threadDAO = new ThreadDAO(jdbcMock);

    static Stream<Arguments> threadDaoTestArguments() {
        return Stream.of(
                Arguments.of(75, 100, 25, false, "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
                Arguments.of(75, 100, 26, true, "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;")
        );
    }

    @ParameterizedTest
    @MethodSource("threadDaoTestArguments")
    void testThreadDao(int threadId, int postId, int limit, boolean desc, String expectedQuery) {
        threadDAO.treeSort(threadId, postId, limit, desc);

        Mockito.verify(jdbcMock).query(
                Mockito.eq(expectedQuery),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any());
    }
}
